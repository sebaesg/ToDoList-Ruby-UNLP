json.extract! task, :id, :list_id, :porcentage, :priority, :startDate, :endDate, :state, :description, :name, :type, :created_at, :updated_at
json.url task_url(task, format: :json)