Rails.application.routes.draw do
  resources :tasks
  resources :lists
    root to: redirect('/about.html')
end
