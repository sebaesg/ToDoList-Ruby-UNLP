class CreateTasks < ActiveRecord::Migration[5.0]
  def change
    create_table :tasks do |t|
      t.belongs_to :list, foreign_key: true
      t.integer :porcentage
      t.integer :priority
      t.date :startDate
      t.date :endDate
      t.integer :state
      t.string :description
      t.string :name
      t.string :type

      t.timestamps
    end
  end
end
